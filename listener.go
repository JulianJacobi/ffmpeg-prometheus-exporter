package main

import (
    "errors"
    "fmt"
    "log"
    "net"
    "strconv"
    "strings"

    "github.com/prometheus/client_golang/prometheus"
)

// UDP listener definition
type Listener struct {
    Name    string
    Address string
    Port    int
}

// Start listening for Listener
func (l Listener) Start() error {
    addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", l.Address, l.Port))
    if err != nil {
        return err
    }

    conn, err := net.ListenUDP("udp", addr)
    if err != nil {
        return err
    }

    log.Printf("Start UDP listener %s on %s:%d", l.Name, l.Address, l.Port)

    for {
        var buf [2048]byte
        n, _, err := conn.ReadFromUDP(buf[0:])
        if err != nil {
            log.Printf("UDP read error at listener %s on %s:%d: %s", l.Name, l.Address, l.Port, err)
            continue
        }
        if n == 0 {
            continue
        }
        status := l.parseFfmpegStatus(string(buf[:n]))

        labels := prometheus.Labels{"ffmpeg":l.Name}

        fps.With(labels).Set(status.Fps)
        bitrate.With(labels).Set(status.Bitrate)
        duplicateFrames.With(labels).Set(status.DupFrames)
        droppedFrames.With(labels).Set(status.DropFrames)
        speed.With(labels).Set(status.Speed)
    }
}

// ffmpeg status representation
type FfmpegStatus struct {
    Fps float64
    Bitrate float64
    DupFrames float64
    DropFrames float64
    Speed float64
}

// parse ffmpeg status packet
func (l Listener) parseFfmpegStatus(m string) FfmpegStatus {
    var status FfmpegStatus
    for _, line := range strings.Split(strings.Trim(m, "\n"), "\n") {
        kv := strings.SplitN(line, "=", 2)
        if len(kv) < 2 {
            log.Printf("Line parsing error at listener %s on %s:%d: %s", l.Name, l.Address, l.Port, line)
            continue
        }
        key := kv[0]
        value := kv[1]
        err := parseFfmpegLine(&status, key, value)
        if err != nil {
                log.Printf("Value parsing failed at listener %s on %s:%d line %s: %s", l.Name, l.Address, l.Port, line, err)
        }
    }
    return status
}

// parse key/value line from ffmpeg status message
func parseFfmpegLine(s *FfmpegStatus, key, value string) error {
    switch key {
    case "fps":
        fps, err := parseFps(value)
        if err != nil {
            return err
        }
        s.Fps = fps
    case "bitrate":
        br, err := parseBitrate(value)
        if err != nil {
            return err
        }
        s.Bitrate = br
    case "dup_frames":
        f, err := parseFrames(value)
        if err != nil {
            return err
        }
        s.DupFrames = f
    case "drop_frames":
        f, err := parseFrames(value)
        if err != nil {
            return err
        }
        s.DropFrames = f
    case "speed":
        sp, err := parseSpeed(value)
        if err != nil {
            return err
        }
        s.Speed = sp
    }
    return nil
}

// parse FPS value
func parseFps(v string) (float64, error) {
   return strconv.ParseFloat(v, 64)
}

// parse bitrate value
func parseBitrate(v string) (float64, error) {
    v = strings.TrimLeft(v, " ")
    unit := strings.TrimLeft(v, "0123456789.")
    baseValue, err := strconv.ParseFloat(strings.TrimSuffix(v, unit), 64)
    if err != nil {
        return 0.0, err
    }
    switch unit {
    case "bits/s":
        return baseValue, nil
    case "kbits/s":
        return baseValue * 1000, nil
    case "Mbits/s":
        return baseValue * 1000000, nil
    case "Gbits/s":
        return baseValue * 1000000000, nil
    default:
        return 0.0, errors.New(fmt.Sprintf("Unknown unit: %s", unit))
    }
}

// parse dupplicate or dropped frames value
func parseFrames(v string) (float64, error) {
    return strconv.ParseFloat(v, 64)
}

// parse speed value
func parseSpeed(v string) (float64, error) {
    return strconv.ParseFloat(strings.TrimRight(v, "x"), 64)
}

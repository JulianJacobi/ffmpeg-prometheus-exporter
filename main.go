package main

import (
    "flag"
    "fmt"
    "log"
    "os"
    "strconv"

    "net/http"

    yaml "gopkg.in/yaml.v3"

    "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
    configPath = flag.String("cf", "config.yml", "Path to config file.")

    fps = prometheus.NewGaugeVec(
        prometheus.GaugeOpts{
            Name: "ffmpeg_fps",
            Help: "Frames per second",
        }, []string{"ffmpeg"},
    )
    bitrate = prometheus.NewGaugeVec(
        prometheus.GaugeOpts{
            Name: "ffmpeg_bitrate",
            Help: "Bitrate",
        }, []string{"ffmpeg"},
    )
    duplicateFrames = prometheus.NewGaugeVec(
        prometheus.GaugeOpts{
            Name: "ffmpeg_dup_frames",
            Help: "Duplicated frames",
        }, []string{"ffmpeg"},
    )
    droppedFrames = prometheus.NewGaugeVec(
        prometheus.GaugeOpts{
            Name: "ffmpeg_drop_frames",
            Help: "Dropped Frames",
        }, []string{"ffmpeg"},
    )
    speed = prometheus.NewGaugeVec(
        prometheus.GaugeOpts{
            Name: "ffmpeg_speed",
            Help: "Encoding speed",
        }, []string{"ffmpeg"},
    )
)

func init() {
    prometheus.MustRegister(fps)
    prometheus.MustRegister(bitrate)
    prometheus.MustRegister(duplicateFrames)
    prometheus.MustRegister(droppedFrames)
    prometheus.MustRegister(speed)
}

type Config struct {
    Address   string     `yaml:"http_address"`
    Port      int        `yaml:"http_port"`
    Path      string     `yaml:"metrics_path"`
    Listeners []Listener `yaml:"listeners"`
}

func main() {
    flag.Parse()

    config := loadConfig()

    log.Print(config)

    for _, l := range config.Listeners {
        go l.Start()
    }

    log.Printf("Start HTTP listener on %s:%d", config.Address, config.Port)
    http.Handle(config.Path, promhttp.Handler())
    log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%d", config.Address, config.Port), nil))
}

// loadConfig from file
func loadConfig() (Config) {
    f, err := os.Open(*configPath)
    if err != nil {
        log.Fatal(err)
    }
    defer f.Close()

    var config = Config{
        Address: "127.0.0.1",
        Port: 9005,
        Path: "/metrics",
    }
    decoder := yaml.NewDecoder(f)
    decoder.KnownFields(true)
    err = decoder.Decode(&config)
    if err != nil {
        log.Fatal("Config file decode errro: ", err)
    }

    for i, l := range config.Listeners {
        if l.Port == 0 {
            log.Fatal("Missing port in listeners config.")
        }
        if l.Address == "" {
            l.Address = config.Address
        }
        if l.Name == "" {
            l.Name = strconv.Itoa(i)
        }
        config.Listeners[i] = l
    }

    return config
}

# ffmpeg Prometheus Exporter

Exports ffmpeg encoding stats in prometheus metrics format.

## How does it work?

Multiple ffmpeg processes can be exposed to prometheus.
Therefore multiple UDP listeners must be configured in
configuration file. Each listener must be configured with
different port and name. The name if added as label `ffmpeg`
to the prometheus metrics.

### ffmpeg configuration

In ffmpeg command a gobal `-progress` option must be set,
pointing to the appropriate udp port.

Example:

    ffmpeg -progress udp://127.0.0.1:12345 -re -f lavfi -i testsrc -c:v libx264 -c:a libfdk_aac -f mpegts /dev/null

## Build

    go build

## Run

    ffmpeg-prometheus-exporter [-cf <path to config file>]

## Configuration

Configuration is stored in `yaml`-formatted config file.
For detailed information take a look at `config.sample.yml`

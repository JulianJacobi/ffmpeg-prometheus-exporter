module gitlab.com/JulianJacobi/ffmpeg-prometheus-exporter

go 1.14

require (
	github.com/prometheus/client_golang v1.9.0
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
